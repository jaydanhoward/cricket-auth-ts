# React Authentication Example

To run the application:
1. first clone the repository
2. Install the dependencies - $ npm install
3. Start mongodb daemon - $ mongod
4. Run the server - $ npm run server
5. Run front dev server in separate terminal - $ npm start
6. Use dev_scripts/createUser.sh to create a user - $ ./dev_scripts/createUser.sh
7. The application should be running at http://localhost:3000 - you can log on with the credentials "user" "password"

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
You can find the most recent version of their guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

Originally forked from https://github.com/faizanv/react-auth-example