import * as React from "react";

interface IProps {}
interface IState {
  message: string;
}

class Secret extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      message: "Loading..."
    };
  }

  public componentDidMount() {
    fetch("/api/secret")
      .then(res => res.text())
      .then(res => this.setState({ message: res }));
  }

  public render() {
    return (
      <div>
        <h1>Secret</h1>
        <p>{this.state.message}</p>
      </div>
    );
  }
}

export default Secret;
